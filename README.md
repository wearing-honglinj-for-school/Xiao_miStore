# xiao_miStore

#### 介绍
使用flask开的一个商场网站，具有注册登录，数据库管理，商品管理包括添加商品，删除商品，查询商品，商品可视化管理。
该项目在基础上开发，其中参考前端部分。
https://github.com/Johnbillll/XiaomiShop-.git
数据可视化参考：
https://gitee.com/mhtccc/51job_visualization



#### 安装教程

1.  下载该项目，并使用pycharm运行
2.  打开navicat，新建一个名为new_shop的数据库，字符集为utf8 -- UTF-8 Unicode，排列规则为utf8_general_ci



![输入图片说明](https://foruda.gitee.com/images/1715935803243865661/e10b2e25_14333618.png "1.png")



3.  运行项目中的sql文件后


![输入图片说明](2.png)




4.完成上面步骤就可以运行app.py

#### 使用说明

1.注册


![输入图片说明](3.png)


2.登录


![输入图片说明](4.png)


3.商品管理,（发布和删除）（数据来自数据库中的product_temp）

删除：


![输入图片说明](5.png)


发布：


![输入图片说明](6.png)


4.将发布的商品展示在主页


![输入图片说明](7.png)


5.商品数据可视化
将手机的价格进行可视化


![输入图片说明](8.png)


###参与贡献
广东科学技术职业学院 计算机学院 人工智能应用技术专业 卢锐城开发 QQ：321502568


